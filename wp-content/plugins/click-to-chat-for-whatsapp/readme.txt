=== Click to Chat ===
Requires at least: 4.6
Tested up to: 5.3.1
Requires PHP: 5.6
Contributors: holithemes
Stable tag: trunk
Tags: whatsapp, whatsapp chat, whatsapp business, whatsapp personal, click to chat, whatsapp group, whatsapp group invite, whatsapp message, social chat, contact us, chat, whatsapp group link, holithemes
License: GPLv2 or later
License URI: https://www.gnu.org/licenses/gpl-2.0.html

WhatsApp connectivity on your WordPress website! Engage customers and generate leads!

== Description ==

Let's make your Web page visitors Contact you through "WhatsApp" with a single Click (WhatsApp Chat, Group, Share)

**Mobile:** Navigates to WhatsApp App.

**Desktop:** Navigates to WhatsApp Webpage (web.whatsapp.com). Users can chat directly from the browser.

[Demo](https://holithemes.com/click-to-chat/list-of-styles/)  

[Documentation](https://holithemes.com/click-to-chat/)


= How easy the plugin is =

* After installing the plugin, add your WhatsApp number ( WhatsApp or WhatsApp business ). 
* If using WhatsApp group invite/chat add WhatsApp group id.


* 9 Styles with one Add any Image/GIF.
* Place the styles at any position of the screen.

= Features =

* Navigates to WhatsApp Chat
* Navigates to WhatsApp Group Chat
* Eight pre-configured Styles (customizable)
* Add Own Image or GIF
* Floating style can position at any place on the screen 
  ( At any pixel of the screen, not limited to fixed positions )
* Show/Hide Styles based on Post type, Post ID, Categories, Device Type
* Pre-filled Message ( Includes Placeholder to add Web page URL )
* Shortcodes to add styles with inline content, floating style
* Google Analytics
* Facebook Analytics

[Google Analytics](https://holithemes.com/click-to-chat/google-analytics/)

[Facebook Analytics](https://holithemes.com/click-to-chat/facebook-analytics/)


= Eight Styles + own Image =

1. **Style-1:** Button, appears as theme button.
1. **Style-2:** Image
1. **Style-3:** Image
1. **Style-4:** Chip style - combination of image and a text message 
1. **Style-5:** Image on hover sidebar content box
1. **Style-6:** Plain text with a link navigates to WhatsApp App or web.whatsapp.com
1. **Style-7:** Icon with customizable padding
1. **Style-8:** Button with text, icon
1. **Style - 99:** Own Image / GIF / Animated image

[list of Styles](https://holithemes.com/click-to-chat/list-of-styles/)

= Pre-filled Message =

Pre-filled message is the text that appears on the WhatsApp chat window when the user clicks on Image/button. User can start the conversation easily. 

To add Web page URL in Pre-filled message add {{url}} placeholder - [pre-filled message](https://holithemes.com/click-to-chat/pre-filled-message/)

by adding the {{url}} placeholder we can understand from which page the user started WhatsApp chat.


= Change Values at page level =

We can change WhatsApp Number, Call to Action at page level

[page level settings](https://holithemes.com/click-to-chat/change-values-at-page-level/)

= Shortcodes =

Shortcodes make the icon appear inline with the content.

Using shortcode attribute we can change the WhatsApp Number, Group Id, Style

== Chat ==

[ht-ctc-chat]

To change number use 'number' attribute

[ht-ctc-chat number=915123456789]

[Shortcodes for Chat](https://holithemes.com/click-to-chat/shortcodes-chat/)

== Group ==

[ht-ctc-group]

To change the Group ID use 'group_id' attribute

[ht-ctc-group group_id=Ica32m8Po7hBhuIDBbsL92]

[Shortcodes for Group](https://holithemes.com/click-to-chat/shortcodes-group/)

== Share ==

[ht-ctc-share]

To change the share text use 'share_text' attribute

[ht-ctc-share share_text="Found Awesome page {{url}}"]

[Shortcodes for Group](https://holithemes.com/click-to-chat/shortcodes-share/)

== screenshots ==

1. 8 Styles, 1 Add own image / GIF Style
1. Global Setting page
1. Chat Setting page
1. Group Setting page
1. Share Setting page
1. Customize Styles
1. Show/Hide settings
1. Change 'Number', 'Call to action', 'Group ID' at page level
1. Shortcodes
1. Style - 5 - Image with content box
1. Style - 8 - Materialize button
1. Style - 99 - Add your own Image / GIF(Animated Image)


== Installation ==

= From Dashboard ( WordPress admin ) =
* plugins -> Add New
* search for 'click to chat for whatsapp'
* click on Install Now  and then Active.

= using FTP or similar =
* unzip Click-to-Chat file and 
* Upload "Click-to-Chat" folder to the "/wp-content/plugins/" directory.
* Activate the plugin through the "Plugins" menu in WordPress.

== Frequently Asked Questions ==

= WhatsApp Number = 
Enter the WhatsApp number with country code ( e.g. 916123456789 ) please dont include +
here in e.g. 91 is country code 6123456789 is mobile number

e.g.
country code +1
number: 6123456789
WhatsApp number: 16123456789

https://www.holithemes.com/whatsapp-chat/whatsapp-number/

= WhatsApp Group Invite/chat = 

Add [WhatsApp group id](https://www.holithemes.com/whatsapp-chat/find-whatsapp-group-id/) at plugin settings page.


= Position of the Styles =

We can change the position values from the plugin settings page.

Add styles at any position of the screen (not fixed to selected positions).

e.g.  
* bottom-right corner
bottom: 10px
right: 10px

* bottom-center
bottom: 10px 
right: 50%

[position to place](https://holithemes.com/click-to-chat/position-to-place/)

= In Mobile, If navigates to Web page =

Plugin Navigates to WhatsApp App on mobile and "web.whatsapp.com" on desktop.

In chat settings check/tick the "Web WhatsApp" option to naviagate directly to web.whatsapp.com on dekstops

= GDPR = 

Plugin won't collect your user data and not using cookies.

Plugin won't collect your data also. 

= Support =

If any problem with the plugin, please create a [new topic](https://wordpress.org/support/plugin/click-to-chat-for-whatsapp/)
or  
send us an email: wp@holithemes.com

= Give Support =

If you like the plugin, please support the developers by giving [5 star rating](https://wordpress.org/support/plugin/click-to-chat-for-whatsapp/reviews/#new-post)


Thanks for showing interest on this plugin.

== Upgrade Notice ==

= using FTP or similar =
* Delete Click-to-Chat folder - your setting will not lost.
* unzip Click-to-Chat file and 
* Upload "Click-to-Chat" folder to the "/wp-content/plugins/" directory.
* Activate the plugin through the "Plugins" menu in WordPress.

= From Dashboard ( WordPress admin ) =
* If plugin new version released - you can see 'update now' link at wp-admin -> plugins
* click on 'update now'

== Changelog ==

= 2.1 =
New interface - docs

= 2.0 =
New interface

= 1.7.2 =
* Style-8 shortcode issue fixed

= 1.7.1 =
* Style-8 simplified

= 1.7 =
* Improved plugin performance
* Style-4 simplified
* Style-1 - default theme button
* Fixed some issues

= 1.6 =
* Google Analytics
* Facebook Analytics

= silent release =
* App First option to fix cache issue
* shortcodes new attribute

= 1.5 =
* Initial message - {{url}} placeholder
* new style - Add own Image
* Hide Style based on Device in shotcodes 

= 1.4 =
* New style-9, Green Square
* Supports Analytics - using google tag manager
* animations - alpha version
* style-3 can choose .svg or .png image versions
* style-4 image size reduced, ~ 2kb
* performance improvement.

= 1.3 =
* can add pre-filled text
* style-1 - can change button text.
* setting page link added in plugins page
* show / hide is now changed to hide on - based on page types

= 1.2 =
* improved: code on how to detect mobile device.
* sticky save button in options page - easy to save.
* option page - Documentation links.

= 1.1 =
* convert plugin to bit of OOP style

= 1.0 =
* Initial release.