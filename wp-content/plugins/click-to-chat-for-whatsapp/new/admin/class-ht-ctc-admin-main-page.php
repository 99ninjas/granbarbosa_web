<?php
/**
 * Main settings page - admin 
 * 
 * this main settings page contains .. 
 * 
 * enable options .. like chat default enabled, group, share, woocommerce
 * 
 * switch option
 * 
 * @package ctc
 * @subpackage admin
 * @since 2.0 
 */

if ( ! defined( 'ABSPATH' ) ) exit;

if ( ! class_exists( 'HT_CTC_Admin_Main_Page' ) ) :

class HT_CTC_Admin_Main_Page {

    public function menu() {

        add_menu_page(
            'Click to Chat - New Interface - Plugin Option Page',
            'Click to Chat',
            'manage_options',
            'click-to-chat',
            array( $this, 'settings_page' ),
            'dashicons-format-chat'
        );
    }

    public function settings_page() {

        if ( ! current_user_can('manage_options') ) {
            return;
        }

        ?>

        <div class="wrap">

            <?php settings_errors(); ?>

            <div class="row">

                <div class="col s12 m12 xl7 options">
                    <form action="options.php" method="post" class="">
                        <?php settings_fields( 'ht_ctc_main_page_settings_fields' ); ?>
                        <?php do_settings_sections( 'ht_ctc_main_page_settings_sections_do' ) ?>
                        <?php submit_button() ?>
                    </form>
                </div>

                <!-- sidebar content -->
                <div class="col s12 m12 xl4 ht-cc-admin-sidebar">
                    <div style="margin-top: 100px; margin-left: 40px;">
                        
                        <div class="row">
                        <div class="col s12">
                            <p>
                            Please let us know if you have any suggestions or feedback!! <br>
                            <a href="http://api.whatsapp.com/send?phone=919494429789&text=Hi HoliThemes, I have a Suggestion/Feedback:" target="_blank">WhatsApp</a> <br>
                            mail: wp@holithemes.com
                            </p>
                        </div>
                        </div>

                        <div class="row">
                        <div class="col s12">
                            <div class="collection with-header">
                                <div class="collection-header"><bold>HoliThemes On</bold></div>
                                    <a target="_blank" href="https://www.facebook.com/holithemes/" class="collection-item">Facebook</a>
                                    <a target="_blank" href="https://twitter.com/holithemes" class="collection-item">Twitter</a>
                                    <a target="_blank" href="https://www.instagram.com/holithemes/" class="collection-item">Instagram</a>
                                    <a target="_blank" href="https://www.youtube.com/channel/UC2Tf_WB9PWffO2B3tswWCGw" class="collection-item">YouTube</a>
                                    <a target="_blank" href="https://www.linkedin.com/company/holithemes" class="collection-item">LinkedIn</a>
                                </div>
                            </div>
                        </div>
                        </div>

                    </div>
                </div>
                
            </div>

        </div>

        <?php

    }


    public function settings() {

        // main settings - options enable .. chat, group, share
        // switch options 
        register_setting( 'ht_ctc_main_page_settings_fields', 'ht_ctc_main_options' , array( $this, 'options_sanitize' ) );

        
        add_settings_section( 'ht_ctc_main_page_settings_sections_add', '', array( $this, 'main_settings_section_cb' ), 'ht_ctc_main_page_settings_sections_do' );
        
        add_settings_field( 'ctc_enable_chat', 'Enable Chat', array( $this, 'ctc_enable_chat_cb' ), 'ht_ctc_main_page_settings_sections_do', 'ht_ctc_main_page_settings_sections_add' );
        add_settings_field( 'ctc_enable_group', 'Enable Group', array( $this, 'ctc_enable_group_cb' ), 'ht_ctc_main_page_settings_sections_do', 'ht_ctc_main_page_settings_sections_add' );
        add_settings_field( 'ctc_enable_share', 'Enable Share', array( $this, 'ctc_enable_share_cb' ), 'ht_ctc_main_page_settings_sections_do', 'ht_ctc_main_page_settings_sections_add' );
        
        add_settings_field( 'ctc_enable_other', 'Enable Features', array( $this, 'ctc_enable_other_cb' ), 'ht_ctc_main_page_settings_sections_do', 'ht_ctc_main_page_settings_sections_add' );
        
        
        $ccw_options = get_option('ccw_options');
		if ( isset( $ccw_options['number'] ) ) {
            // display this setting page only if user switched from previous interface.. ( for new users no switch option )
            register_setting( 'ht_ctc_main_page_settings_fields', 'ht_ctc_switch' , array( $this, 'options_sanitize' ) );
            add_settings_field( 'ht_ctc_switch', '', array( $this, 'ht_ctc_switch_cb' ), 'ht_ctc_main_page_settings_sections_do', 'ht_ctc_main_page_settings_sections_add' );
		}
        
    }

    public function main_settings_section_cb() {
        ?>
        <h1>Click to Chat ( New Interface )</h1>
        <?php
    }


    // Enable WhatsApp Chat Features
    function ctc_enable_chat_cb() {

        $options = get_option('ht_ctc_main_options');

        if ( isset( $options['enable_chat'] ) ) {
        ?>
        <p>
            <label>
                <input name="ht_ctc_main_options[enable_chat]" type="checkbox" value="1" <?php checked( $options['enable_chat'], 1 ); ?> id="enable_chat" />
                <span>Enable WhatsApp Chat Features</span>
            </label>
            <p class="description">  - <a href="<?php echo admin_url( 'admin.php?page=click-to-chat-chat-feature' ); ?>">Chat Settings page</a> </p>
        </p>
        <?php
        } else {
            ?>
            <p>
                <label>
                    <input name="ht_ctc_main_options[enable_chat]" type="checkbox" value="1" id="enable_chat" />
                    <span>Enable WhatsApp Chat Features</span>
                </label>
            </p>
            <?php
        }
        ?>
        <p class="description">  - <a target="_blank" href="https://www.holithemes.com/click-to-chat/enable-chat">more info</a> </p>

        <?php

    }


    // Enable WhatsApp Group Features
    function ctc_enable_group_cb() {

        $options = get_option('ht_ctc_main_options');

        if ( isset( $options['enable_group'] ) ) {
        ?>
        <p>
            <label>
                <input name="ht_ctc_main_options[enable_group]" type="checkbox" value="1" <?php checked( $options['enable_group'], 1 ); ?> id="enable_group" />
                <span>Enable Group Features</span>
            </label>
            <p class="description">  - <a href="<?php echo admin_url( 'admin.php?page=click-to-chat-group-feature' ); ?>">Group Settings page</a> </p>
        </p>
        <?php
        } else {
            ?>
            <p>
                <label>
                    <input name="ht_ctc_main_options[enable_group]" type="checkbox" value="1" id="enable_group" />
                    <span>Enable Group Features</span>
                </label>
            </p>
            <?php
        }
        ?>
        <p class="description">  - <a target="_blank" href="https://www.holithemes.com/click-to-chat/enable-group">more info</a> </p>

        <?php

    }


    // Enable WhatsApp Share Features
    function ctc_enable_share_cb() {

        $options = get_option('ht_ctc_main_options');

        if ( isset( $options['enable_share'] ) ) {
        ?>
        <p>
            <label>
                <input name="ht_ctc_main_options[enable_share]" type="checkbox" value="1" <?php checked( $options['enable_share'], 1 ); ?> id="enable_share" />
                <span>Enable Share Features</span>
            </label>
            <p class="description">  - <a href="<?php echo admin_url( 'admin.php?page=click-to-chat-share-feature' ); ?>">Share Settings page</a> </p>
        </p>
        <?php
        } else {
            ?>
            <p>
                <label>
                    <input name="ht_ctc_main_options[enable_share]" type="checkbox" value="1" id="enable_share" />
                    <span>Enable Share Features</span>
                </label>
            </p>
            <?php
        }
        ?>
        <p class="description">  - <a target="_blank" href="https://www.holithemes.com/click-to-chat/enable-share">more info</a> </p>


        <br>
        <?php

    }




    // Enable Features
    function ctc_enable_other_cb() {

        $options = get_option('ht_ctc_main_options');

        ?>

        <ul class="collapsible">
        <li>
        <div class="collapsible-header">Enable Other features</div>
        <div class="collapsible-body">

        <?php


        // Google Analytics
        if ( isset( $options['google_analytics'] ) ) {
            ?>
            <p>
                <label>
                    <input name="ht_ctc_main_options[google_analytics]" type="checkbox" value="1" <?php checked( $options['google_analytics'], 1 ); ?> id="google_analytics" />
                    <span>Google Analytics</span>
                </label>
            </p>
            <?php
        } else {
            ?>
            <p>
                <label>
                    <input name="ht_ctc_main_options[google_analytics]" type="checkbox" value="1" id="google_analytics" />
                    <span>Google Analytics</span>
                </label>
            </p>
            <?php
            }
            ?>
            <p class="description">If Google Analytics installed creates an Event there - <a target="_blank" href="https://www.holithemes.com/click-to-chat/google-analytics/">more info</a> </p>
            <br>

        <?php



        // Facebook Analytics
        if ( isset( $options['fb_analytics'] ) ) {
            ?>
            <p>
                <label>
                    <input name="ht_ctc_main_options[fb_analytics]" type="checkbox" value="1" <?php checked( $options['fb_analytics'], 1 ); ?> id="fb_analytics" />
                    <span>Facebook Analytics</span>
                </label>
            </p>
            <?php
            } else {
                ?>
                <p>
                    <label>
                        <input name="ht_ctc_main_options[fb_analytics]" type="checkbox" value="1" id="fb_analytics" />
                        <span>Facebook Analytics</span>
                    </label>
                </p>
                <?php
            }
            ?>
            <p class="description"> If Facebook Analytics installed - creates an Event there - <a target="_blank" href="https://www.holithemes.com/click-to-chat/facebook-analytics/">more info</a> </p>

            </div>
            </div>
            </li>
            <ul>

            <?php
    }



    // switch interface
    function ht_ctc_switch_cb() {
        $options = get_option('ht_ctc_switch');
        $interface_value = esc_attr( $options['interface'] );
        ?>
        <!-- <br><br><br><br><br><br><br><br> -->
        <ul class="collapsible">
        <li>
        <div class="collapsible-header">Switch Interface</div>
        <div class="collapsible-body">

        <p class="description">If you are convenient with the previous interface in comparison to the new one, please switch to previous interface</p>
        <br><br>
        <div class="row">
            <div class="input-field col s12" style="margin-bottom: 0px;">
                <select name="ht_ctc_switch[interface]" class="select-2">
                    <option value="no" <?php echo $interface_value == 'no' ? 'SELECTED' : ''; ?> >Previous Interface</option>
                    <option value="yes" <?php echo $interface_value == 'yes' ? 'SELECTED' : ''; ?> >New Interface</option>
                </select>
                <label>Switch Interface</label>
            </div>
        <!-- <p class="description">If you are convenient with the previous interface in comparison to the new one, please switch to previous interface</p> -->
        </div>

        </div>
        </div>
        </li>
        <ul>

        <?php
    }

    
    


    /**
     * Sanitize each setting field as needed
     *
     * @since 2.0
     * @param array $input Contains all settings fields as array keys
     */
    public function options_sanitize( $input ) {

        if ( ! current_user_can( 'manage_options' ) ) {
            wp_die( 'not allowed to modify - please contact admin ' );
        }

        $new_input = array();

        foreach ($input as $key => $value) {
            if( isset( $input[$key] ) ) {
                $new_input[$key] = sanitize_text_field( $input[$key] );
            }
        }


        return $new_input;
    }


}

$ht_ctc_admin_main_page = new HT_CTC_Admin_Main_Page();

add_action('admin_menu', array($ht_ctc_admin_main_page, 'menu') );
add_action('admin_init', array($ht_ctc_admin_main_page, 'settings') );

endif; // END class_exists check
